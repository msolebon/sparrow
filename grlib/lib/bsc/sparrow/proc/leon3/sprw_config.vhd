library ieee;
use ieee.std_logic_1164.all;

package sprw_config is
    constant SPRW_VLEN : integer := 32; -- Vector length
end package;
