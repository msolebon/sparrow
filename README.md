# SPARROW

## Project description 

This project presents SPARROW, a low-cost accelerator for Artificial Intelligence (AI) specially tailored for space processors. SPARROW is a Single-Instruction Multiple-Data (SIMD) module which is dived in two stages, the first one for parallel vector computations and the second one for reductions. It has a low hardware overhead thanks to the re-utilization of the integer register file which acts as a vector register file. Additionally, a special register, SPARROW Control Register (%SCR), has been included to set a mask and swizzling for further versatility.
This commit corresponds to version 2.0 of SPARROW, check git tags to use an older release.

SPARROW is portable and has been implemented for the LEON3 and NOEL-V space processors. Both processors are distributed under a GPL license by Cobham Gaisler in the GRLIB library. This repository contains said library but with just a subset of the FPGA designs where SPARROW has been implemented and tested. Also is included a new design for the Xilinx Zynq Ultrascale+ FPGA which is not available in the official GRLIB release.

Cross-compiler tools are provided in their respective repositories with support for the SPARROW instructions. SPARROW compilers must be considered as part of this project and are available under the same license (see below).

- LEON3 GCC-based: [BCC](https://gitlab.bsc.es/msolebon/bcc-sparrow/-/tree/dev)
- NOEL-V GCC-based: [NCC](https://gitlab.bsc.es/msolebon/ncc-sparrow)
- LEON3 & NOEL-V: [LLVM](https://gitlab.bsc.es/msolebon/llvm-sparrow/-/tree/dev)

For SPARROW v2.0, make sure to be on `dev` branch in BCC and LLVM. NCC implementation for SPARROW v1.0 is not available.
When the `dev` branches are merged with the main, a tag will be set to use the v1.0 version.

## File organization

The repository has a main directory named `grlib`, which contains the source files of GRLIB GPL 2021.2 library. The relevant directories for this projects are:

- `grlib/designs`: Contains the top level entities for simulation and implementation (see Usage).
- `grlib/lib`: The different components of the library, the files for SPARROW are located in `grlib/lib/bsc`.

Any modification done in the original files is tagged with a **sparrow** comment to simplify it's localization.

## Usage 

To generate SPARROW for the LEON3 or NOEL-V no additional work is required, SPARROW can be enabled/disabled in `config.vhd` file for each design or by modifying the parameters in the design top file.

### Simulation

Simulation has been tested with GHDL (GCC-backend). In the top level directory use `make ghdl` to synthesize the design and `make ghdl-run` to execute it. Other alternatives exists (see `grlib/bin/Makefile`). For debug is recommended to use `make ghdl-fst` with a file `signals.txt` listing the signals to be debugged.

- Simulate LEON3 with *leon3-minimal* design
- Simulate NOEL-V with *noelv-generic* design

### FPGA Implementation

A LEON3 design on a Zynq Ultrascale+ (ZCU102), which is not supported in the official GRLIB release, is included which has been tested with SPARROW.
Similarly, a NOEL-V implementation for the Virtex Ultrascale+ (VCU118) has been tested with SPARROW with 32 and 64 bits support.
No other existing design has been tested with SPARROW but it is expected to work with any/minimal modifications.

In the top level directory use `make vivado` to synthetise the design in xilinx devices.

**NOTE**: At this moment the LEON3 implementation for the Virtex Ultrascale+ and the NOEL-V implementation for the Zynq Ultrascale+ are work-in-progress.

To execute the core with SPARROW on the FPGA is recommended the use of GRMON for loading and debugging the program. GRMON is not included in this project but is available in evaluation/academic license [here](https://www.gaisler.com/index.php/downloads/debug-tools).

### Tests and code generation

No benchmark are available for the v2.0 of SPARROW, please, use those of v1.0 and modify them accordingly.

## Credits and License

This project has been developed by Marc Solé Bonet under the supervision of Dr. Leonidas Kosmidis. The work was presented as Master Thesis at the Universitat Politècnica de Catalunya (UPC) for the Master in Innovation and Research in Informatics (MIRI) - High Performance Computing (HPC). This project has been developed while working at the Barcelona Supercomputing Center (BSC).

The Thesis has been awarded by the Spanish Chapter of the IEEE AESS as best Master Thesis. Also, a prior iteration of this project received the first place at the [Xilinx Open Hardware 2021](http://www.openhw.eu/2021) competition in the Student category.

The project is open source under a GPL license. If you use this work or any part of it, please cite it as:  

*Marc Solé Bonet, Leonidas Kosmidis, SPARROW: A Low-Cost Hardware/Software Co-designed SIMD Microarchitecture for AI Operations in Space Processors. Design, Automation and Test in Europe Conference, DATE 2022*

## Contact
If you require support or want to contact us, send an e-mail to: [marc.solebonet@bsc.es](mailto:marc.solebonet@bsc.es) and [leonidas.kosmidis@bsc.es](mailto:leonidas.kosmidis@bsc.es).
